import tkinter as tk


# 创建一个Frame来定义窗口,并加入控件
class Application(tk.Frame):

    # 初始化
    def __init__(self,master=None):
        tk.Frame.__init__(self,master)

        # 显示窗口并使用grid布局
        self.grid()

        # 调用下面属性创建控件
        self.createWidgets()

    # 创建quitButton属性
    def createWidgets(self):

        # command为点击时调用的函数
        self.quitButton = tk.Button(self, text='退出', command=self.quit)
        self.helloLabel = tk.Label(self, text='Hello, world!')

        # 显示窗口并使用pack布局
        self.helloLabel.pack()
        self.quitButton.pack()

        self.okButton = tk.Button(self, text='OK', command=self.ok)
        self.okButton.pack()

        self.nameInput = tk.Entry(self)
        self.nameInput.pack()

    def ok(self):
        name = self.nameInput.get()
        self.inputLabel = tk.Label(self,text=name)
        self.inputLabel.grid()



# 创建一个Application对象app
app = Application()

# 设置窗口标题
app.master.title('First Tkinter')

# 主循环开始,主循环会一直执行，直到出现退出请求
# command是Frame.quit即退出,所以点击后就结束了
app.mainloop()