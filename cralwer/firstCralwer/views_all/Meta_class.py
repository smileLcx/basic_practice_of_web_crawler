from abc import ABCMeta, abstractmethod


# 抽象基类是只能被继承而不会被实例化的类
class Vehicle(metaclass=ABCMeta):

    @abstractmethod
    def refill_tank(self, litres):
        pass

    @abstractmethod
    def move_ahead(self):
        pass


class Truck(Vehicle):
    def __init__(self, company, color, wheels):
        self.company = company
        self.color = color
        self.wheels = wheels

    def refill_tank(self, litres):
        pass

    def move_ahead(self):
        pass

if __name__ == '__main__':
    mini = Truck("Tesla Roadster", "Black", 4)
    print(mini.company)