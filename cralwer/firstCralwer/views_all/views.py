from django.shortcuts import render
import requests,sys,time
from bs4 import BeautifulSoup

# Create your views here.


# 向指定网站发送get请求,并返回
# r = requests.get('https://www.douban.com/')
# print(r.text)


# post请求
# p = requests.post('https://www.douban.com/',data={'p1':1,'p2':2})


# BeautifulSoup对象实例化,并使用lxml进行解析
# soup = BeautifulSoup(r.text,'lxml')

# 查找第一个a标签
# find = soup.find('a')

# find_all( name , attrs , recursive , string , **kwargs )
# name 参数：可以查找所有名字为 name 的tag
# attr 参数：就是tag里的属性
# string 参数：搜索文档中字符串的内容
# recursive 参数:调用tag的 find_all()方法时，Beautiful Soup会检索当前tag的所有子孙节点。
#   如果只想搜索tag的直接子节点，可以使用参数recursive=False

# 获取其中所有的p标签中 属性class为market-group-topic-footer的
# finds = soup.find_all('p',class_='market-group-topic-footer')


# 下面给定一个例子来爬取douban网首页的图片链接
# 给指定一个请求头来模拟chrome浏览器,User-Agent可在检查浏览器页面中获取
headers = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'}
web_url = 'https://www.douban.com/'
r = requests.get(web_url,headers=headers)

all = BeautifulSoup(r.text,'lxml').find_all('img')
# for img in all:
#     print(img)



# 练习
url = 'https://book.douban.com/latest?icn=index-latestbook-all'
html = requests.get(url).text
soup = BeautifulSoup(html,'lxml')

book = soup.find_all('h2')
for x in range(3,book.__len__()):
    if book[x].text == '非虚构类  · · · · · ·' or book[x].text == '虚构类  · · · · · ·':
        pass
    else:print(book[x].text)

pl = soup.find_all('p',class_='detail')



# 抓取二进制文件保存
url = 'https://www.crummy.com/software/BeautifulSoup/bs4/doc/_images/6.1.jpg'

r = requests.get(url)
path = sys.path[1] + "/static/123.jpg"
with open(path, 'wb') as f:
    # r.text返回的是二进制数据解码后的字符串,会是乱码。r.content为解码前的二进制数据
    f.write(r.content)
print("success")