import urllib.request
import urllib.parse
import socket



data = {"userid": "kamihati"}

# 对字典进行url编码,urlencode是专用于网络请求的通用编码方式
a = urllib.parse.urlencode(data)

# Python3中，bytes()通常用于网络数据传输、二进制图片和文件的保存等
data_bytes = bytes(a, encoding='utf8')

# 使用python自带的标准库urllib来获取HTML。若指定参数,则请求的类型将变为POST
response = urllib.request.urlopen('http://httpbin.org/post', data=data_bytes)

# read()方法用于用于解析HTML内容
print(response.read().decode('utf-8'))
print(response.code)
print(response.url)


# timeout设置链接时长
# 由于没有站点能够达到0.1秒就返回结果的速度。所以这条语句必定抛出time out的异常
try:
    response = urllib.request.urlopen('http://httpbin.org/get', timeout=0.1)

except urllib.error.URLError as e:
    if isinstance(e.reason, socket.timeout):
        print("TIME OUT")



# 模拟浏览器发送请求
data = dict(a=1, b='哈哈')
data_bytes = bytes(urllib.parse.urlencode(data), encoding='utf8')

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
           'Host': 'httpbin.org'}

req = urllib.request.Request(url='http://httpbin.org/post', data=data_bytes, headers=headers, method='POST')
# 若接收请求的url并不支持post方式的请求。则这里可能会抛出405,Not Allowed异常
response = urllib.request.urlopen(req)
print(response.read().decode("utf-8"))