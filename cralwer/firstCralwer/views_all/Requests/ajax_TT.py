import requests
from urllib.parse import urlencode
import os,sys
from hashlib import md5
from multiprocessing.pool import Pool


# 分析Ajax爬取今日头条街拍美图
api_1 = 'https://www.toutiao.com/api/pc/feed/?category=%E7%BB%84%E5%9B%BE&utm_source=toutiao&max_behot_time=0&as=A1C54CC2E27CD73&cp=5C229C6D77D3EE1&_signature=b-pA6gAAM8vFrei.y4hdcG.qQP'
api_2 = 'https://www.toutiao.com/stream/widget/local_weather/data/'

def get_page(offset):
    params = {
        'offset': offset,
        'format': 'json',
        'keyword': '街拍',
        'autoload': 'true',
        'count': '20',
        'cur_tab': '1',
    }
    url = 'http://www.toutiao.com/search_content/?' + urlencode(params)
    try:
        res = requests.get(url)
        if res.status_code == 200:
            return res.json()
    except:
        return None


def get_images(json):
    '''
    :param json:
        从get_page中获取到的json数据
    :return:
        重新整理过的图片列表
    '''
    if json.get('data'):
        for item in json.get('data'):
            title = item.get('title')
            images = item.get('image_list')
            for image in images:
                yield {
                    'image': image.get('url'),
                    'title': title
                }

path = sys.path[1] + "/static/pic/"
print('=======',path)

def save_image(item):
    if not os.path.exists(path+item.get('title')):
        # makedirs()可以一次创建多级目录，哪怕中间目录不存在也能正常的（替你）创建
        os.makedirs(path+item.get('title'))
    try:
        # 请求这个图片链接，获取图片的二进制数据
        res = requests.get('https:'+item.get('image'))

        # 图片的名称可以使用其内容的MD5值，这样可以去除重复。
        file_path = '{0}/{1}.{2}'.format(item.get('title'), md5(res.content).hexdigest(), 'jpg')

        if not os.path.exists(path+file_path):
            with open(path+file_path, 'wb') as f:
                f.write(res.content)
            print('下载路径:%s' % (path+file_path))
        else:
            print('Already Downloaded', path+file_path)
    except Exception as e:
        print('，item %s' % item)


def main(offset):
    json = get_page(offset)

    for item in get_images(json):
        print(item)
        save_image(item)


GROUP_START = 1
GROUP_END = 20

if __name__ == '__main__':
    # 多进程
    pool = Pool()
    groups = ([x * 20 for x in range(GROUP_START, GROUP_END + 1)])  # 偏移量,通过分析ajax得到间隔为20
    pool.map(main, groups)  # 设置进程,进程数
    pool.close()
    pool.join()
    # main(20)