import requests.cookies
import sys
import urllib3
import logging
import IPy


# 文件的上传
def file_up():
    files = {"file": open(sys.path[1] + "/static/123.jpg", 'rb')}
    r = requests.post("http://httpbin.org/post", files=files)
    print(r.text)



headers = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',
           'Host':'www.zhihu.com'}

def jar():
    # 先从浏览器中得到登陆后的cookie
    cookies = '_zap=817a0b08-632c-401d-be5a-71fcbd047535; _xsrf=298ac95c-b2e7-41ed-b405-70c37f3ee2e7; d_c0="ALBhRv8UsA6PTrH3wm1etI5jG_OTkOOXMWI=|1545114832"; l_n_c=1; l_cap_id="M2ZlYjI1OTYxMGRiNDFhZGI2ODZjYmNiNzg1Zjg0MDA=|1545284560|a4eeca887e589f5ef672bac42f9a1761ad845415"; r_cap_id="MDExM2RjMTFmNGM2NDYzNmJhOGRiMjIxZGNjMWY5MTc=|1545284560|b1f25669038abaeec6b6f2e7d97c6a1888670b6b"; cap_id="ZDVjZDU2MzFhMDAyNDMzNGI0ZDhjMTgwNDc0YWRmYzE=|1545284560|f2d968e75e3f96fbec2bbe9c769fe9efdbd27b87"; n_c=1; tgw_l7_route=53d8274aa4a304c1aeff9b999b2aaa0a; capsion_ticket="2|1:0|10:1545287244|14:capsion_ticket|44:NDFhZTc1YWJkMTc4NDQ2NDgwNWM1NmU1N2FiNTAwZGE=|db1a205d119ad0a261c8e3229eb1422da0313f00d904bd3450e8af41ad35e83b"'

    # 再把cookie按照key,value的方式加入RequestsCookieJar实例中
    jar = requests.cookies.RequestsCookieJar()
    for cookie in cookies.split(';'):
        key, value = cookie.split('=', 1)
        jar.set(key, value)

    return jar


# 模拟知乎登录(仅是伪装成登录状态,并没有实际的登录)
def up():
    r = requests.get('https://www.zhihu.com/people/ai-bo-11/following',cookies=jar(), headers=headers)
    print(r.status_code)
    print(r.text.find("几字微言"))



def hold_session():
    """
    会话维持,正常情况下两次请求相当于两个不同浏览器打开的两个网页。
    使用session可以让两次请求使用同一个会话信息
    :return:
    """
    s = requests.session()
    # 设置cookie
    s.get('http://httpbin.org/cookies/set/number/123456789')
    # 读取cookie
    r = s.get('http://httpbin.org/cookies')
    print(r.text)


def use_cookie_hold_session():
    """
    会话保持。知乎登录状态伪装
    :return:
    """
    s = requests.session()
    s.cookies = jar()
    s.headers = headers

    # 访问首页。若有写文章的文字则说明已登录
    r = s.get('https://www.zhihu.com')
    print(r.text.find("写文章"))

    # 再访问个人资料编辑页面。若能访问则说明已登录
    r = s.get('https://www.zhihu.com/people/edit')
    print(r.text.find('茅十八'))


def ssl_verify():
    """
    有些https站点使用的CA证书。不是被CA机构认可的证书这种站点在访问时会抛出SSLError
    :return:
    """
    # 可通过捕获警告写入日志的方式忽略警告
    logging.captureWarnings(True)

    # 也可以直接忽略警告
    urllib3.disable_warnings()

    # 使用verify屏蔽对ssl的检测,虽然不会抛出异常,但会发出警告(使用上面的方式可以消除)
    response = requests.get("https://uu898.com", verify=False)

    print(response.status_code)
    print(response.text)

# 计算可用IP地址数
def ip(ip):
    i = IPy.IP(ip)
    print(i.len())
    for x in i:
        print(x)

if __name__ == '__main__':
    ssl_verify()

    ip('192.168.0.0/24')
    # /24表示 二进制的掩码前24位都是1 即:11111111.11111111.11111111.00000000