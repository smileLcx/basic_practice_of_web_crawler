import requests
import lxml.html
import json
# https://blog.csdn.net/weixin_42670402/article/details/82385716
# https://germey.gitbooks.io/python3webspider/content/4.1-XPath%E7%9A%84%E4%BD%BF%E7%94%A8.html
# https://1024dada.com/python/q/166507

def xPath():
    url = 'https://1024dada.com/python/q/166507'

    # 获取 url 网页源码
    content = requests.get(url).content

    # 将网页源码转换为 XPath 可以解析的格式
    html = lxml.html.etree.HTML(content)

    # 可能为列表或节点 //选取所有子孙元素  /选取所有子级元素  text()获取文本
    element =html.xpath("//*[@class='content-slider']/div//h1/text()")
    print(element)

    # 当有多个class属性时需要用contains()  /@..获取指定的属性内容
    e = html.xpath('//div[contains(@class, "tran")]//a/@href')
    print(e)


# 爬取淘宝评论并生成词云
def taobao(url):
    if url[url.find('id=') + 14] != '&':
        id = url[url.find('id=')+3:url.find('id=')+15]
    else:
        id = url[url.find('id=') + 3:url.find('id=') + 14]

    url = 'https://rate.taobao.com/feedRateList.htm?auctionNumId=' + id + '&currentPageNum=1'
    res = requests.get(url)
    # strip() 方法用于移除字符串开头/结尾指定的字符
    jc = json.loads(res.text.strip().strip('()'))
    max = jc['total']

    comments = []
    count = 0
    page = 1
    print('该商品共有评论' + str(max) + '条,具体如下: loading...')

    while count<max:
        res = requests.get(url[:-1] + str(page))
        page = page + 1
        jc = json.loads(res.text.strip().strip('()'))
        j = jc['comments']

        for x in j:
            comments.append(x['content'])
            print(comments[count])
            count = count + 1
    return comments


if __name__ == '__main__':
    xPath()

    # 宝贝链接:
    goods = 'https://item.taobao.com/item.htm?id=545932731550&ali_refid=a3_430406_1007:1102722587:N:4509868274773381721_0_447972609024:49d528fedc09b99c40c3a03621c076a9&ali_trackid=1_49d528fedc09b99c40c3a03621c076a9&spm=a21bo.2017.201874-sales.14'
    taobao(goods)