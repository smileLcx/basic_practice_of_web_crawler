import requests
from bs4 import BeautifulSoup
import time
import datetime

def proxy_set():
    """
    使用代理。
    :return:
    """
    proxies = {
        # "http": "185.132.133.99:1080",

        # 'http': 'http://user:password@host:port',  # 使用需要验证Http Base Auth的代理 需要指定用户名,密码,代理端口和ip

        'http': 'socks5://61.129.70.109:1080', # 使用socks协议代理,需要安装socks库:  pip install 'requests[socks]'
    }
    response = requests.get("https://www.taobao.com", proxies=proxies,timeout=2)
    print(response.status_code)

    all = BeautifulSoup(response.text, 'lxml').find_all('a')
    for x in all:
        print(x)


def t():
    l = time.localtime(1545906600)
    t = time.asctime(l)
    print(t)

    tt = time.strftime("%Y-%m-%d %H:%M:%S", l)
    print(tt)

def s(e):
    t_list = e.split(' ')
    t_y = t_list[0].split('-')
    t_t = t_list[1].split(':')
    print(t_t)

    t1 = datetime.datetime(int(t_y[0]),int(t_y[1]),int(t_y[2]),int(t_t[0]),int(t_t[1]),int(t_t[2]))
    t0 = datetime.datetime(1970,1,1,8,0,0)
    print(type(t1))
    tt = t1 - t0
    t = tt.total_seconds()
    print(t)
if __name__ == '__main__':
    # proxy_set()
    t()
    s('2018-12-07 15:20:00')