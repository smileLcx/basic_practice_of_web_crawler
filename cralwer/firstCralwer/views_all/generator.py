# 格式化输出
def f():
    a = 'lala'
    b = 42
    c = 2

    print(f'{a} age is {b/c}')
    print('{} age is {:-9.2%}'.format(a , b/c))
    print('the age white {1} years boy is {0}, he has {c} sister'.format(a , b/c ,c='two'))

    # 不保留小数点
    print('42 / 2 = %2.0f' %(b/c))
    print('42 %s 2 等于 %d'%('除以',b/c))




# 参考:https://www.ibm.com/developerworks/cn/opensource/os-cn-python-yield/
def fab_list(max):
   n, a, b = 0, 0, 1
   L = []
   while n < max:
       L.append(b)
       a, b = b, a + b
       n = n + 1
   return L     # 函数在运行中占用的内存会随着参数 max 的增大而增大


# 使用可迭代 class 的方式会有效减少内存的占用,但代码不够简洁
class Fab(object):
    def __init__(self, max):
        self.max = max
        self.n, self.a, self.b = 0, 0, 1

    def __iter__(self):
        return self

    def next(self):
        if self.n < self.max:
            r = self.b
            self.a, self.b = self.b, self.a + self.b
            self.n = self.n + 1
            return r
        raise StopIteration()


# 一个带有 yield 的函数就是一个生成器,函数被调用时不会执行任何函数代码，直到对其调用 next()（在 for 循环中会自动调用 next()）才开始执行。
# 在 for 循环执行时，每次循环都会执行 fab 函数内部的代码，执行到 yield b 时，fab 函数就返回一个迭代值
# 下次迭代时，代码从 yield b 的下一条语句继续执行，而函数的本地变量看起来和上次中断执行前是完全一样的，于是函数继续执行，直到再次遇到 yield。

# 使用 generator(生成器) 的好处:即保持了代码的简洁,同时节省内存的占用
def fab(max):
    n, a, b = 0, 0, 1
    while n < max:
        yield b
        # print(b) 函数中用 print 打印数字会导致该函数可复用性较差
        a, b = b, a + b
        n = n + 1


if __name__ == '__main__':
    f = fab(5)
    print(f.__next__())
    print(f.__next__())
    print(f.__next__())

    print('_'*5)

    for x in f:
        print(x)