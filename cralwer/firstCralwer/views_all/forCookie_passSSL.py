import urllib.request
import ssl
from http import cookiejar




# 全局取消证书验证
def s(e):
    ssl._create_default_https_context = ssl._create_unverified_context
    r = urllib.request.urlopen(e)
    respanse = r.read().decode("utf-8")
    print(respanse)


# 参考:https://germey.gitbooks.io/python3webspider/content/3.1.1-%E5%8F%91%E9%80%81%E8%AF%B7%E6%B1%82.html
# 获取cookie
def c(e):
    cookie = cookiejar.CookieJar()
    hanler = urllib.request.HTTPCookieProcessor(cookie)
    opener = urllib.request.build_opener(hanler)

    ssl._create_default_https_context = ssl._create_unverified_context

    r = opener.open(e)
    for x in cookie:
        print(x.name,' = ',x.value)


if __name__ == '__main__':
    # https协议的网站通常是需要SSL验证的
    url_1 = 'http://image.so.com/z?ch=beauty'
    url_2 = "https://1024dada.com/list/5-1013-1/1"

    s(url_2)

    c(url_1)
    print('-'*10)
    c(url_2)

