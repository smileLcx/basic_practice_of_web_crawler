# super() 函数是用于调用父类(超类)的一个方法。

class FooParent(object):

    def __init__(self):
        self.parent = 'I\'m the parent.'
        print('Parent')

    def bar(self, message):
        print("%s from Parent" % message)




class FooChild(FooParent):

    def __init__(self):
        super(FooChild, self).__init__() #先找到其 父类 并调用相应方法, 再执行子类的
        print('Child')

    def toBinary(self,num):
        # bin()将一个十进制整数转为 int或者long int 的二进制表示
        b = bin(num)
        print(b)

    def bar(self, message):
        super(FooChild, self).bar(message)
        print('Child bar fuction')
        print(self.parent)


from functools import reduce
# 和JS中reduce()用法一样,累积计算
def fond():
    num = reduce(lambda x,y: x+y, [1, 2, 3, 4, 5])

    sentences = ['The Deep Learning textbook is a resource intended to help students and practitioners enter the field of machine learning in general and deep learning in particular. ']
    sun = reduce(lambda a,x: a+x.count("learning"), sentences, 0)

    # print(sentences[0].count('learning'))
    return num,sun


class Dict_(object):
    aa = 123

    def __init__(self,a):
        self.a = a

    def dict_(self):
        a = self.a
        b = 2
        return locals(),vars()

    @classmethod
    def no_self(cls):
        print(cls)
        return '不需要实例化类就可以被类本身调用'

def x(e):
    return e + 1



if __name__ == '__main__':
    fooChild = FooChild()
    fooChild.bar('HelloWorld')

    fooChild.toBinary(123)

    print('\n---',chr(97)) # 十进制字符
    print(chr(0x61)) # 十六进制

    print('1~5的和为{},'.format(fond()[0]),'learning出现的次数为{}.\n'.format(fond()[1]))

    # 局部名称空间代表在函数执行时候定义的所有名字,返回该局部名称空间的字典
    print(locals())
    print(Dict_('lala').dict_()[0])
    print(Dict_('lala').dict_()[1])

    # 返回对象__dict__的内容，无论是类对象还是实例对象,内建对象没有__dict__属性会报TypeError
    print(vars(Dict_))
    if vars(Dict_) == Dict_.__dict__:
        print('Ture')

    print(Dict_.no_self())

    # 反回一个全局变量的字典，包括所有导入的变量
    print('\n', globals())

    # 返回一个哈希值,所得的结果不仅和对象的内容有关，还和对象的 id()也就是内存地址有关
    print('\n',hash('123'))