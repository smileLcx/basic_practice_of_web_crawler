# -*- coding:utf-8 -*-
from Crypto import Random
from Crypto.Cipher import AES
import time


# AES key, must be either 16, 24, or 32 bytes

# 这里key和 iv 都使用长度为16字节的 随机字符串
key = str(int(time.time() * 1000000))
iv = Random.new().read(AES.block_size)  # block_size = 16

raw = input('请输入要加密的数字：')

cipher = AES.new(key, AES.MODE_CFB, iv)
data = iv + cipher.encrypt(raw)
print("加密后的数据长度：") # 为什么20个字节长度，不应该是16的整数倍吗?#因为，mode=AES.MODE_CFB
print(len(data))
print("加密后的数据为：")
print(data)

iv = data[:16]
cipher = AES.new(key, AES.MODE_CFB, iv)
data1 = cipher.decrypt(data[16:])
print("解密后的数据为：")
print(data1)
datastr = str(data1, 'UTF-8')
print("解密后的数字为：",eval(datastr))
print("解密后的数据类型为：",type(eval(datastr)))