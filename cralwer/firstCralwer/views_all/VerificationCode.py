# -*- coding:utf-8 -*-  

"""
*  使用 Python 生成字母验证码图片
"""

import random
from PIL import Image, ImageDraw, ImageFont,ImageFilter

def randomColor():
    r = random.randint(0, 256)
    g = random.randint(0, 256)
    b = random.randint(0, 256)
    return (r, g, b)

# 随机大小写26字母:
def randomChar():
    root = ""
    a = ord('a')
    A = ord('A')
    for i in range(0, 26):
        root += chr(a + i)
        root += chr(A + i)
    x = random.choice(root)
    return x

# 随机大写26字母:
def rndChar():
    return chr(random.randint(65, 90))


# 创建Font对象,设置字体样式
font = ImageFont.truetype("Arial.ttf", 52) # 创建字体对象给ImageDraw中的text函数使用

ans = ""
for i in range(4):
    ans += randomChar()
    # ans += rndChar()

# 随机颜色
bg_color = randomColor()

bgImg = Image.new('RGB', (170, 60), bg_color) # 新建一个图片对象, 宽,高固定 背景颜色随机
canvas = ImageDraw.Draw(bgImg)

# 填充每个像素:
for x in range(170):
    for y in range(60):
        canvas.point((x, y), fill=randomColor())

# 把随机字母写入图片(写入位置,内容,颜色,样式)
canvas.text((20, 0),ans,(255,255,255), font)

# 模糊滤镜
bgImg = bgImg.filter(ImageFilter.BLUR)

# 存入指定路径
bgImg.save('../../static/YZ.jpg','jpeg')
rstImg = Image.open('../../static/YZ.jpg')
rstImg.show()

inp = input('Please type in the characters in the image:')
if  inp == ans:
    print('验证成功')
else:
    print('验证失败')


# 参考文档
# https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000/0014320027235877860c87af5544f25a8deeb55141d60c5000