from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import time


# 自动化测试工具
# 必须在浏览器的开发菜单中启用“允许远程自动化”选项，以便通过WebDriver控制浏览器


# 声明浏览器对象(Selenium支持非常多的浏览器，如Safari、Chrome、Firefox、Edge等，以及手机端的浏览器)
browser = webdriver.Safari()
try:
    # 打开百度首页
    browser.get('https://www.baidu.com')

    # 查找到输入框的节点位置(find_elements可获取多个节点组成的列表)
    input = browser.find_element_by_id('kw')

    # 输入 美女图片 并搜索
    input.send_keys('美女图片')
    input.send_keys(Keys.ENTER)

    # 间隔3秒后清空输入框
    time.sleep(3)
    input.clear()

    # 获取百度一下按钮节点
    button = browser.find_elements(By.CLASS_NAME,'s_btn')
    but = browser.find_element(By.ID, 'su')
    print('--->>>',button)
    print('--->>>',but)

    # 输入Python并搜索
    input.send_keys('Python')
    input.send_keys(Keys.ENTER)
    time.sleep(5)

    # 新打开一个网站 间隔2秒后 退回
    browser.get('https://gitee.com/')
    time.sleep(2)
    browser.back()

    # 延时等待,确保节点已经加载出来
    wait = WebDriverWait(browser, 10)
    wait.until(EC.presence_of_element_located((By.ID, 'content_left')))

    print(browser.current_url)
    print(browser.get_cookies())
    # print(browser.page_source)

except Exception as e:
    print(e)

finally:
    # finally是无论是否有异常，最后都要做的一些事情。
    # 在try语句中含有return的情况下，并不会阻碍finally的执行。

    browser.close() #关闭浏览器
